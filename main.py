import ConfigParser
import Adafruit_DHT
import smtplib
import time
import math

# DHT22 sensor connected to GPIO23
sensor = Adafruit_DHT.DHT22
pin = 23

# Read some settings from settings.ini
config = ConfigParser.ConfigParser()
config.read("settings.ini")

def send_alert_mail():
    # Connect to SMTP server and send mail with alert message
    server = smtplib.SMTP(config.get("Mail", "smtp_server"))
    server.ehlo()
    server.starttls()
    server.login(config.get("Mail", "smtp_user"), config.get("Mail", "smtp_password"))
    server.sendmail(config.get("Mail", "mail"), config.get("Mail", "recipient"), config.get("Mail", "text"))
    server.quit()

print "Daemon started"

while True:
    # Try to grab a sensor reading.  Use the read_retry method which will retry up
    # to 15 times to get a sensor reading (waiting 2 seconds between each retry).
    humidity, temperature_c = Adafruit_DHT.read_retry(sensor, pin)

    # Note that sometimes you won't get a reading and
    # the results will be null (because Linux can't
    # guarantee the timing of calls to read the sensor).
    # If this happens try again!
    if humidity is not None and temperature_c is not None:
        print humidity, temperature_c
        temperature_f = temperature_c * 1.8 + 32

        # Write sensor values to file
        with open(config.get("WebServer", "filename"), "w") as f:
            f.write(str(math.floor(temperature_f)) + " " + str(math.floor(humidity)))
    else:
        print "Can't read data from sensor"

    # Wait 10 seconds before next reading
    time.sleep(10)