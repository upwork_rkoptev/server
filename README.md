# Setup instructions

1. Clone this repository to your PI
```sh
$ git clone https://rkoptev@bitbucket.org/upwork_rkoptev/server.git
$ cd server
```
2. Install python module to communicate with DHT22 sensor
```sh
$ sudo apt-get update
$ sudo apt-get install build-essential python-dev
$ git clone https://github.com/adafruit/Adafruit_Python_DHT.git
$ cd Adafruit_Python_DHT
$ sudo python setup.py install
$ cd ~
```
After executing this commands in your home folder will remain "Adafruit_Python_DHT" folder. You can safely remove it as you don't need it anymore.
3. Install Apache web server and PHP to host web pages
```sh
$ sudo apt-get install apache2 -y
$ sudo apt-get install php5 libapache2-mod-php5 -y
```
4. Copy "web" folder to Apache web server folder and make it accessible for python program
```sh
$ cp web/* /var/www/html/
$ sudo chown -R pi /var/www/html
```
5. Add python program to rc.local file (This will automatically run the script at startup)
```sh
$ sudo nano /etc/rc.local
```
Append this line to file:
```sh
python /home/pi/server/main.py &
```
After that press F2 and then Enter to save and close file
6. Reboot your PI
```sh
$ sudo reboot
```

7. Done
Now you can type IP address of Raspberry in your browser and see actual temperature and humidity from sensor