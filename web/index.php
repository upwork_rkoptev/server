<?php

$page = file_get_contents("template.html");
$values = file_get_contents("values.txt");
// If file is empty - exit from program
if ($values === false) {
	echo "Data is not available";
	exit();
}

// Get appropriate values from file
$values = split(" ", $values);
$temperature_f = $values[0];
$humidity = $values[1];
$page = str_replace("{{temperature}}", $temperature_f . "&deg;F", $page);
$page = str_replace("{{humidity}}", $humidity . "%", $page);
echo $page;

?>